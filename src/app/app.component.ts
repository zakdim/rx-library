import { Component, OnInit } from '@angular/core';
import {
  Observable,
  from,
  interval,
  fromEvent
} from 'rxjs';
import { ajax } from 'rxjs/ajax';
import { take } from 'rxjs/operators';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'rx-library';

  private baseUrl = 'https://storerestservice.azurewebsites.net/api/products/';

  constructor() { }

  ngOnInit(): void {
    // this.observableFromPromise();
    // this.observableFromCounter();
    // this.observableFromEvent();
    // this.observableAjaxRequest();

  }

  observableFromPromise() {
    // Create an Observable out of a promise
    console.log('Observable from promise');
    const data = from(fetch(this.baseUrl));
    // Subscribe to begin listening for async result
    data.subscribe({
      next(response) { console.log(response); },
      error(err) { console.error('Error: ' + err); },
      complete() { console.log('Completed'); }
    });

  }

  observableFromCounter() {
    console.log('Observable from counter');
    // Create an Observable that will publish a value on an interval
    // const secondsCounter: Observable<number> = interval(1000);
    const secondsCounter: Observable<number> = interval(1000).pipe(take(5));
    // Subscribe to begin publishing values
    const subscription = secondsCounter.subscribe(
      n => console.log(`It's been ${n + 1} seconds since subscribing!`),
      error => console.error('Error: ' + error),
      () => console.log('Completed!')
    );
  }

  observableFromEvent() {
    const el = document.getElementById('my-element');

    // Create an Observable that will publish mouse movements
    const mouseMoves = fromEvent(el, 'mousemove');

    // Subscribe to start listening for mouse-move events
    const subscription = mouseMoves.subscribe((evt: MouseEvent) => {
      // Log coords of mouse movements
      console.log(`Coords: ${evt.clientX} X ${evt.clientY}`);

      // When the mouse is over the upper-left of the screen,
      // unsubscribe to stop listening for mouse movements
      if (evt.clientX < 40 && evt.clientY < 40) {
        subscription.unsubscribe();
        console.log('Unsubscribed');
      }
    });
  }

  observableAjaxRequest() {
    console.log('Observable AJAX request');

    // Create an Observable that will create an AJAX request
    const apiData = ajax(this.baseUrl);
    // Subscribe to create the request
    apiData.subscribe(res => console.log(res.status, res.response));
  }
}
