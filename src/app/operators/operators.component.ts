import { Component, OnInit } from '@angular/core';

import { Observable, of, pipe } from 'rxjs';
import { map, filter } from 'rxjs/operators';

@Component({
  selector: 'app-operators',
  templateUrl: './operators.component.html',
  styleUrls: ['./operators.component.css']
})
export class OperatorsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    // this.mapOperator();
    // this.pipeFunction();
    this.observablePipe();
  }

  mapOperator() {
    console.log('Map operator');

    const nums: Observable<number> = of(1, 2, 3);

    const squareValues = map((val: number) => val * val);
    const squaredNums = squareValues(nums);

    squaredNums.subscribe(x => console.log(x));

    // Logs
    // 1
    // 4
    // 9
  }

  pipeFunction() {
    console.log('Pipe function');

    const nums = of(1, 2, 3, 4, 5);

    // Create a function that accepts an Observable.
    const squareOddVals = pipe(
      filter((n: number) => n % 2 !== 0),
      map(n => n * n)
    );

    // Create an Observable that will run the filter and map functions
    const squareOdd = squareOddVals(nums);

    // Subscribe to run the combined functions
    squareOdd.subscribe(x => console.log(x));
  }

  observablePipe() {
    console.log('Observable pipe');

    const squareOdd = of(1, 2, 3, 4, 5)
      .pipe(
        filter(n => n % 2 !== 0),
        map(n => n * n)
      );

    // Subscribe to get values
    squareOdd.subscribe(x => console.log(x));
  }
}
